package application.admin.api.domain;

public record RepositoryResponseData(
        String repositoryType,
        String projectName,
        String javaVersion,
        String springVersion
) {
}
