package application.admin.api.parsers;

import application.admin.api.domain.RepositoryResponseData;

public interface Parser {

    RepositoryResponseData parse(String contents, String projectName, String repositoryType);

}
