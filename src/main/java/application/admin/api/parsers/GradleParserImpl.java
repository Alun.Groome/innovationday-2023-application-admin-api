package application.admin.api.parsers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import application.admin.api.domain.RepositoryResponseData;

public class GradleParserImpl implements Parser {

    public RepositoryResponseData parse(String fileContent, String projectName, String repositoryType) {

        String javaVersion = "";
        String springVersion = "";

        javaVersion = getPath("sourceCompatibility\\s*=\\s*(.*)", fileContent);

        if (javaVersion.equals("") || javaVersion == null) {
            javaVersion = getPath("JavaLanguageVersion\\.of\\((\\d+)\\)", fileContent);
        }

        springVersion = getPath("springBootVersion\\s*=\\s*'(.*)'", fileContent);

        return new RepositoryResponseData(repositoryType, projectName, javaVersion, springVersion);
    }

    private String getPath(String regex, String fileContent) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(fileContent);
        String output = "";

        if (matcher.find()) {
            output = matcher.group(1).trim();
        }
        return output;
    }

}
