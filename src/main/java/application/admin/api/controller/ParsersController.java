package application.admin.api.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import application.admin.api.domain.RepositoryResponseData;
import application.admin.api.service.ParsingServiceInterface;

@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
public class ParsersController {

    private final ParsingServiceInterface parsingServiceInterface;

    public ParsersController(ParsingServiceInterface parsingServiceInterface) {
        this.parsingServiceInterface = parsingServiceInterface;
    }

    @GetMapping("getRepositoryData/{groupId}")
    ResponseEntity<List<RepositoryResponseData>> getRespositoryData(@PathVariable String groupId) {
        List<RepositoryResponseData> repositoryResponseDataList = parsingServiceInterface.getRepositoryResponseData(groupId);
        if (repositoryResponseDataList == null || repositoryResponseDataList.size() == 0) {
            return new ResponseEntity<>(repositoryResponseDataList, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(repositoryResponseDataList, HttpStatus.OK);
    }
}
