package application.admin.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationAdmin {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationAdmin.class, args);
    }

}
