package application.admin.api.service;

import application.admin.api.parsers.GradleParserImpl;
import application.admin.api.parsers.MavenParserImpl;
import application.admin.api.parsers.Parser;

public class ParsingFactory {

    private ParsingFactory() {}

    public static Parser getParser(String repositoryType) {
        return "GRADLE".equals(repositoryType) ? new GradleParserImpl() : new MavenParserImpl();
    }

}
