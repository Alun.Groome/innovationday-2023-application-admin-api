package application.admin.api.service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.GroupApi;
import org.gitlab4j.api.RepositoryApi;
import org.gitlab4j.api.models.Group;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.RepositoryFile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import application.admin.api.domain.RepositoryResponseData;

@Service
public class ParsingServiceImpl implements ParsingServiceInterface {

    private GitLabApi gitLabApi;
    private RepositoryApi repositoryAPI;
    private GroupApi groupApi;
    List<RepositoryResponseData> repositoryResponseDataList = new ArrayList<>();

    @Value("${gitlab.host.url}")
    private String gitlabHostUrl;
    @Value("${gitlab.personal.access.token}")
    private String gitlabAccessToken;

    @Override
    public List<RepositoryResponseData> getRepositoryResponseData(String groupId) {
        connectToGitlab();
        try {
            Group group = groupApi.getGroup(groupId);

            String fileContents = "";
            RepositoryResponseData repositoryResponseData;
            List<Project> projects = group.getProjects();

            for(Project project : projects) {
                String projectName = project.getName();
                fileContents = getFileContents(project, "pom.xml");

                if(fileContents != null) {
                    // parse the xml file if not equal to null
                    repositoryResponseData = ParsingFactory.getParser("MAVEN").parse(fileContents, projectName, "MAVEN");
                    repositoryResponseDataList.add(repositoryResponseData);
                    continue;
                }
                fileContents = getFileContents(project, "build.gradle");
                if(fileContents != null) {
                    // parse the xml file if not equal to null
                    repositoryResponseData = ParsingFactory.getParser("GRADLE").parse(fileContents, projectName, "GRADLE");
                    repositoryResponseDataList.add(repositoryResponseData);
                    continue;
                }
                repositoryResponseDataList.add(new RepositoryResponseData("UNKNOWN", projectName, "", ""));
            }

        } catch (GitLabApiException gitLabApiException) {
            return Collections.emptyList();
        }

        return repositoryResponseDataList;
    }

    private void connectToGitlab() {
        gitLabApi = new GitLabApi(gitlabHostUrl, gitlabAccessToken);
        repositoryAPI = gitLabApi.getRepositoryApi();
        groupApi = gitLabApi.getGroupApi();
    }

    private String getFileContents(Project project, String filePath) {
        String defaultBranch = project.getDefaultBranch();

        String filecontent = "";

        try{
            RepositoryFile file = gitLabApi.getRepositoryFileApi().getFile(project.getId(), filePath, defaultBranch);
            filecontent = file.getContent();
            if ("base64".equals(file.getEncoding().toString())) {
                byte[] decodedBytes = Base64.getDecoder().decode(filecontent);
                filecontent = new String(decodedBytes, StandardCharsets.UTF_8);
            }
        } catch (GitLabApiException gitLabApiException) {
            return null;
        }
        return filecontent;
    }
}
