package application.admin.api.service;

import java.util.List;

import application.admin.api.domain.RepositoryResponseData;

public interface ParsingServiceInterface {

    List<RepositoryResponseData> getRepositoryResponseData(String groupId);

}
